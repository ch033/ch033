#include <stdio.h>
int main()
{
    int n, i, num, f;
    printf("Enter size of array: ");
    scanf("%d", &n);
    int arr[n];
    printf("Enter elements in array: ");
    for(i=0; i<n; i++)
    {
        scanf("%d", &arr[i]);
    }

    printf("\nEnter element to search: ");
    scanf("%d", &num);
   
    for(i=0; i<n; i++)
    {
       
        if(arr[i] == num)
        {
            f = 1;
            break;
        }
    }
    if(f == 1)
    {
        printf("\n%d is found at index %d", num, i);
    }
    else
    {
        printf("\n%d is not found in the array", num);
    }
    return 0;
}

