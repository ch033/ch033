#include<stdio.h>
#include<math.h>
int main()
{
	int a,b,c;
	float det,root1,root2;
	printf("enter the co efficients of Quadratic Equation");
	scanf("%d%d%d",&a,&b,&c);
	det=(b*b)-(4*a*c);
	if (det>0)
	{
		printf("roots are real and distinct\n");
		root1=((-b)+sqrt((b*b)-(4*a*c)))/2*a;
		root2=((-b)-sqrt((b*b)-(4*a*c)))/2*a;
		printf("roots of quadratic equation are=%f\t%f",root1,root2);
	}
	else if (det<0)
	{
		printf("roots are imaginary\n");
		
	}
	else 
	{
		
		printf("roots are equal\n");
		root1=((-b)+sqrt((b*b)-(4*a*c)))/2*a;
		root2=((-b)-sqrt((b*b)-(4*a*c)))/2*a;
		printf("roots of quadratic equation are=%f\t%f",root1,root2);
	}
		return 0;
}