#include<stdio.h>
int main()
{
    struct NAME
    {
        char first_name[20];
        char middle_name[20];
        char last_name[20];
    };
    struct DOJ
    {
        int day;
        int mon;
        int year;
    };
    struct employee
    {
        int emp_id;
        struct NAME emp_name;
        float emp_salary;
        struct DOJ emp_join_date;
    };
    struct employee emp1;
    printf("ENTER THE EMPLOYEE DETAILS\n");
    printf("ENTER EMPLOYEE ID:\n");
    scanf("%d",&emp1.emp_id);
    printf("ENTER EMPLOYEE's FIRST NAME:\n");
    scanf("%s", emp1.emp_name.first_name);
    printf("ENTER EMPLOYEE's MIDDLE NAME:\n");
    scanf("%s", emp1.emp_name.middle_name);
    printf("ENTER EMPLOYEE's LAST NAME:\n");
    scanf("%s", emp1.emp_name.last_name);
    printf("ENTER EMPLOYEE SALARY:\n");
    scanf("%f", &emp1.emp_salary);
    printf("ENTER DATE OF JOINING:\n");
    scanf("%d %d %d", &emp1.emp_join_date.day, &emp1.emp_join_date.mon, &emp1.emp_join_date.year);
    printf("EMPLOYEE DETAILS:\n");
    printf("EMPLOYEE ID: %d\n",emp1.emp_id);
    printf("EMPLOYEE NAME: %s %s %s \n",emp1.emp_name.first_name, emp1.emp_name.middle_name, emp1.emp_name.last_name);
    printf("EMPLOYEE SALARY: %f\n",emp1.emp_salary);
    printf("DATE OF JOINING: %d/%d/%d\n",emp1.emp_join_date.day, emp1.emp_join_date.mon, emp1.emp_join_date.year);
    return 0;
}       